Source: pseudo
Section: utils
Priority: optional
Maintainer: Andrej Shadura <andrewsh@debian.org>
Build-Depends:
 attr,
 debhelper-compat (= 12),
 libattr1-dev,
 libsqlite3-dev,
 python3,
Standards-Version: 4.5.0
Homepage: https://www.yoctoproject.org/software-item/pseudo/
Vcs-Browser: https://salsa.debian.org/debian/pseudo
Vcs-Git: https://salsa.debian.org/debian/pseudo.git

Package: libpseudo
Architecture: any
Multi-Arch: same
Replaces:
 pseudo (<< 1.7.5-3~),
Breaks:
 pseudo (<< 1.7.5-3~),
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: advanced tool for simulating superuser privileges
 The pseudo utility offers a way to run commands in a virtualized "root"
 environment, allowing ordinary users to run commands which give the
 illusion of creating device nodes, changing file ownership, and otherwise doing
 things necessary for creating distribution packages or filesystems.
 .
 Pseudo has a lot of similarities to fakeroot but is a new implementation
 that improves on the problems seen using fakeroot. Pseudo is now
 extensively used by Poky as a replacement to fakeroot but can also be
 used standalone in many other use cases.
 .
 This package contains the LD_PRELOAD libraries.

Package: pseudo
Architecture: any
Multi-Arch: foreign
Depends:
 libpseudo (>= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Replaces:
 libpseudo (<< 1.9.0+git20190515+996bead-2~)
Breaks:
 libpseudo (<< 1.9.0+git20190515+996bead-2~)
Provides:
 fakeroot,
Description: advanced tool for simulating superuser privileges
 The pseudo utility offers a way to run commands in a virtualized "root"
 environment, allowing ordinary users to run commands which give the
 illusion of creating device nodes, changing file ownership, and otherwise doing
 things necessary for creating distribution packages or filesystems.
 .
 Pseudo has a lot of similarities to fakeroot but is a new implementation
 that improves on the problems seen using fakeroot. Pseudo is now
 extensively used by Poky as a replacement to fakeroot but can also be
 used standalone in many other use cases.
